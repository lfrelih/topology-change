# Topology Change

Script that outputs changes of routes when topology changes.

## Install

`pip install -r requirements.txt`

## Example use

`python3 topology_change.py 50 -t example/Links_t0.yml example/Links_t1.yml -e A B`

the first argument is the stream throughput which is simmulated for every possible combination of nodes provided by topology files

with option `-t` provide 2 topology files that contain topology information before and after topology change. Or provide one file to calculate all routes and streams

Option `-v` will print the python object after calculation with data from all links in topologies

