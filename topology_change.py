import networkx as nx
import itertools
import yaml
import argparse
import pprint

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)

def calc_streams(g: nx.Graph, stream_throughput, exclude_nodes) -> dict:
    d = dict()
    nodes = [node for node in g.nodes if node not in exclude_nodes]
    for source, destination in itertools.permutations(nodes, 2):
            shortest_paths = nx.all_shortest_paths(g, source, destination, weight='weight')
            try:
                paths = [p for p in shortest_paths]
            except nx.NetworkXNoPath:
                continue
            
            traffic = stream_throughput/len(paths) # ECMP load balancing
            d[source+destination] = list()
            for path in paths:
                d[source+destination].append(path)
                for n1, n2 in pairwise(path):
                    g[n1][n2]['total'] += traffic
                    g[n1][n2]['streams'].append({'source': source, 'destination': destination, 'traffic': traffic, 'ecmp-num': len(paths)-1})
    return d

def read_yaml_file(filename):
    with open(filename, 'r') as file:
        data = yaml.safe_load(file)
    return data

def read_data(file) -> nx.Graph:
    data = read_yaml_file(file)
    links = data['Links']
    g = nx.DiGraph()
    for link in links:
        g.add_edge(link['source'], link['destination'], weight=link['cost'], bandwidth=link['bandwidth'], total=0, streams=[])
    return g

def print_compare_streams(s0: dict, s1: dict):
    for stream in set(list(s0.keys()) + list(s1.keys())):
        try:
            new_path = s1[stream]
        except KeyError:
            print(f'Stream {stream} no longer exists')
        try:
            path = s0[stream]
        except KeyError:
            print(f'New stream {stream} appeared with path {new_path}')
        if path != new_path:
            print(f'Stream {stream} changed from {path} to {new_path}')

def print_streams(s: dict):
    for stream, path in s.items():
        print(f'Stream {stream} with path {path}')
        

def print_compare_graphs(g0: nx.Graph, g1: nx.Graph):  
    for src, dst in set(list(g0.edges) + list(g1.edges)):
        try:
            data1 = g1[src][dst]
        except KeyError:
            data1 = {'total': 0, 'bandwidth': 0, 'streams': []}
            extra_text = ' - new link'
        try:
            data0 = g0[src][dst]
        except KeyError:
            data0 = {'total': 0, 'bandwidth': 0, 'streams': []}
            extra_text = ' - link no loger exists'
        ratio0 = int(data0['total']/data0['bandwidth']*100)
        ratio1 = int(data1['total']/data1['bandwidth']*100)
        if ratio0 != ratio1:
            losing0 = max(data0["total"] - data0["bandwidth"], 0)
            losing1 = max(data1["total"] - data1["bandwidth"], 0)
            extra_text = f' - traffic saved {losing0-losing1}' if losing0 > 0 or losing1 > 0 else ''
            print(f'Link {src+dst} total traffic changed from {ratio0}% ({len(data0["streams"])} streams) to {ratio1}% ({len(data1["streams"])} streams) {extra_text}')

def print_graph(g: nx.Graph):
    for src, dst, data in g.edges(data=True):
        ratio = int(data['total']/data['bandwidth']*100)
        losing = max(data["total"] - data["bandwidth"], 0)
        print(f'Link {src+dst} total traffic {ratio}% ({len(data["streams"])} streams)')

if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser('topology_change')
    arg_parser.add_argument('traffic', type=int, help='Symmetric stream throughput (all possible combinations of streams between nodes with this throughput)')
    arg_parser.add_argument('-t', nargs='+', type=str, help='Topology files. Provide 2 space seperated files for comparison or a single file to calculate', default=['Links_t0.yml', 'Links_t1.yml'])
    arg_parser.add_argument('-e', nargs='+', type=str, help='List of nodes that are excluded from being the source of target of a stream', default=[''])
    arg_parser.add_argument('-v', action='store_true', help='Verbose mode')

    args = arg_parser.parse_args()

    if len(args.t) == 1:
        g_t0 = read_data(args.t[0])
        streams0 = calc_streams(g_t0, args.traffic, args.e)
        print_graph(g_t0)
        print_streams(streams0)
        if args.v:
            print(f'Links dump for {args.t[0]}:')
            pprint.pprint(list(g_t0.edges(data=True)))
    elif len(args.t) == 2:
        g_t0 = read_data(args.t[0])
        g_t1 = read_data(args.t[1])

        streams0 = calc_streams(g_t0, args.traffic, args.e)
        streams1 = calc_streams(g_t1, args.traffic, args.e)

        print_compare_graphs(g_t0, g_t1)
        print_compare_streams(streams0,streams1)
        if args.v:
            print(f'Links dump for {args.t[0]}:')
            pprint.pprint(list(g_t0.edges(data=True)))
            print(f'Links dump for {args.t[1]}:')
            pprint.pprint(list(g_t1.edges(data=True)))
    else:
        print('Provide 2 files for comparison or a single file for calculation')
        exit(1)